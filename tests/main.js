// Import.
const assert = require('assert');
const Conf = require('../index');

// Constants.
const TEST_CONF_FILES_DIRECTORY = './tests/data';
const TEST_ENVIRONMENT_CONFIG_PREFIX = 'CONFIG_';

// Prepare global variables.
const describe = global.describe;
const it = global.it;

// Init conf.
const conf = Conf.get(TEST_CONF_FILES_DIRECTORY, TEST_ENVIRONMENT_CONFIG_PREFIX);

// Main tests.
describe('Check config objects.', () => {
  // Test if only user-defined config exist.
  it('Should contain key "1".', () => {
    assert.strictEqual('1', conf.test1.key);
  });

  // Check if default and user-defined configs exists.
  it('Should contain key "2".', () => {
    assert.strictEqual('2', conf.test2.key);
  });

  // Check if only default config exist.
  it('Should contain key "3-default".', () => {
    assert.strictEqual('3-default', conf.test3.key);
  });

  // Check environment config.
  it('Should contain key 44.', () => {
    assert.strictEqual(44, conf.test4.key);
  });

  // Log.
  console.log(conf);
});

describe('Check default config objects.', () => {
  const defaultConf = Conf.getDefault(TEST_CONF_FILES_DIRECTORY);

  // Test if only user-defined config exist.
  it('Should not contain test1.', () => {
    assert.strictEqual(undefined, defaultConf.test1);
  });

  // Check if default and user-defined configs exists.
  it('Should contain default key "2".', () => {
    assert.strictEqual('2-default', defaultConf.test2.key);
  });

  // Check if only default config exist.
  it('Should contain key "3-default".', () => {
    assert.strictEqual('3-default', defaultConf.test3.key);
  });

  // Check environment config.
  it('Should not contain test4.', () => {
    assert.strictEqual(undefined, defaultConf.test4);
  });
});