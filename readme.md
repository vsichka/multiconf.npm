# Multiconf

![](https://gitlab.com/vsichka/multiconf.npm/raw/main/logo/logo.png)

[![NPM Downloads][downloads-image]][downloads-url]

## Description

Simple management of your JSON configurations. Access your `*.json` files (user-defined configs) within a specific directory. You can also access `*.json.default` files (default configs), but only if a user-defined config with the same name does not exist.

## Installation

```sh
npm install multiconf
```

## Initialization

```js
// Import.
const Multiconf = require('multiconf');

// Init Multiconf.
const conf = Multiconf.get('./conf');
// Remember that you can use this method without params - default
// conf directory path will be used ("./conf").
```

## Sample config files

`./conf/server.json.default`:

```json
{
	"hostname": "localhost",
	"port": 3141,
	"token": "<removed>"
}
```

`./conf/server.json`:

```json
{
	"hostname": "localhost",
	"port": 3141,
	"token": "QWERTY-123456"
}
```

## Using

```js
// Show "localhost:3141".
console.log(conf.server.hostname + ':' + conf.server.port);

// Show "Token is QWERTY-123456".
console.log('Token is ' + conf.server.token);
// Remember that if user-defined config ("./conf/server.json") not
// exist - default config ("./conf/server.json.default") will be
// used.
```

## JSONC support

You can also use JSONC (JSON with comments) format for your config files. For example, `./conf/server.jsonc`:

```jsonc
{
	// Server hostname.
	"hostname": "localhost",
	// Server port.
	"port": 3141,
	// Server token.
	"token": "QWERTY-123456"
}
```

JSONC has more priority than JSON. If you have `server.jsonc` and `server.json` - `server.jsonc` will be used.

## License

The MIT License (MIT)

Copyright © 2017 - 2024 Volodymyr Sichka

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

[downloads-image]: https://img.shields.io/npm/dm/multiconf.svg
[downloads-url]: https://npmjs.org/package/multiconf
